token = $('meta[name="csrf-token"]').attr('content');


function update_current_list(page) {
    $.ajax({
        url: 'update-current-list?page='+page,
        type: 'post',
        data: {_token: token},
        success: function (data) {
            if (data != 0) {
                $('.current-body').html(data);
                $('.alert-content').html('')
            }else{
                $('.alert-content').html('' +
                    '<div class="alert nav-item alert-warning m-auto">\n' +
                    '    <strong>Warning!</strong> The data was not updated, please reload the page.\n' +
                    '</div>');
            }
        }
    });
}

function update_current_list_time() {
    setTimeout(function () {
        var page = $('.page-item.active').find('.page-link').attr('href').split('page=')[1];
        update_current_list(page);
        update_current_list_time()
    }, 15000);
}

$('.update-data').on('click', function () {
    var page = $('.page-item.active').find('.page-link').attr('href').split('page=')[1];
    update_current_list(page);
});

update_current_list_time();

$(function() {
    $(document).on('click', '.pagination .page-link', function(e) {
        e.preventDefault();
        $('.pagination .page-item').removeClass('active');
        $(this).parents('.page-item').addClass('active');
        var page = $(this).attr('href').split('page=')[1];
        update_current_list(page);

    });
});



