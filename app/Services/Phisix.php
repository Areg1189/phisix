<?php

namespace App\Services;


use GuzzleHttp\Client;
use Illuminate\Pagination\LengthAwarePaginator;

class Phisix
{

    public static function get($request)
    {
        try {
            $url = env('PHISIX_URL');
            $client = new Client();
            $result = $client->get($url);
            $array = json_decode($result->getBody()->getContents())->stock??'';
            $data = is_array($array) ? $array : [];
            return self::pagination($request, $data, 50);

        } catch (\Exception $e) {
            return false;
        }

    }

    public static function pagination($request, array $data, int $paginate)
    {
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        $itemCollection = collect($data);

        $currentPageItems = $itemCollection->slice(($currentPage * $paginate) - $paginate, $paginate)->all();

        $data = new LengthAwarePaginator($currentPageItems, count($itemCollection), $paginate);

        $data->setPath($request->url());

        return $data;
    }
}
