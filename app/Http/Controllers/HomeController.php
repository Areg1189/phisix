<?php

namespace App\Http\Controllers;

use App\Services\Phisix;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $data = Phisix::get($request);
//dd($data);
        $alert = $data ? '' : 1;

        return view('home', compact('data', 'alert'));
    }

    public function update_current_list(Request $request)
    {

        if ($request->ajax()) {
            $data = Phisix::get($request);
            if ($data) {
                return View::make('partials.current-list', compact('data'));
            }
            return 0;
        }
    }


}
