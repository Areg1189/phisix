@if($data)
    @foreach($data as $item)
        <tr>
            <td>{{$item->name}}</td>
            <td>{{(int)$item->volume}}</td>
            <td>{{number_format($item->price->amount,2,',','')}}</td>
        </tr>
    @endforeach
@endif
