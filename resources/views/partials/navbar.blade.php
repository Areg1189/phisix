<nav class="navbar navbar-expand-sm bg-light fixed-top">
    <!-- Links -->
    <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="{{route('home')}}">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">About</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="#">Products</a>
        </li>
    </ul>
    <div class="alert-content">
        @if($alert)
            <div class="alert nav-item alert-warning m-auto">
                <strong>Warning!</strong> An error occurred with Api Reload the page.
            </div>
        @endif
    </div>

    <ul class="navbar-nav ml-auto">
        <button class="btn update-data">Update</button>
    </ul>

</nav>
