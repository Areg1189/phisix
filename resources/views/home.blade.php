@extends('layouts.app')

@section('content')
    <h2 class="mt-5">Current List</h2>
    <table class="table mt-5">
        <thead>
        <tr>
            <th>name</th>
            <th>volume</th>
            <th>amount</th>
        </tr>
        </thead>
        <tbody class="current-body">
        @include('partials.current-list',['data' => $data])
        </tbody>
    </table>
    @if($data)
        <div class="d-flex justify-content-center">
            {{$data->links()}}
        </div>
    @endif
@stop
